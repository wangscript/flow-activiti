package com.flow.module.process.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.flow.module.process.dao.BizApplySimpleDao;
import com.flow.module.process.pojo.BizApplySimple;
import com.flow.module.process.service.BizApplySimpleService;
import com.flow.module.sys.service.FlowProcessService;
import com.flow.module.sys.service.FlowTaskService;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameNoUtil;
import com.system.comm.utils.FrameTimeUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * biz_apply_simple的Service
 * @author autoCode
 * @date 2017-12-29 10:14:11
 * @version V1.0.0
 */
@Component
public class BizApplySimpleServiceImpl implements BizApplySimpleService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BizApplySimpleServiceImpl.class);
	/** 流程定义的name */
	private static final String PROCESS_NAME = "simpleProcess";
	@Autowired
	private BizApplySimpleDao bizApplySimpleDao;
	@Autowired
	private FlowProcessService flowProcessService;
	@Autowired
	private FlowTaskService flowTaskService;

	@Override
	public BizApplySimple get(String id) {
		return bizApplySimpleDao.get(id);
	}

	@Override
	public ResponseFrame pageQuery(BizApplySimple bizApplySimple) {
		bizApplySimple.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = bizApplySimpleDao.findBizApplySimpleCount(bizApplySimple);
		List<BizApplySimple> data = null;
		if(total > 0) {
			data = bizApplySimpleDao.findBizApplySimple(bizApplySimple);
		}
		Page<BizApplySimple> page = new Page<BizApplySimple>(bizApplySimple.getPage(), bizApplySimple.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public ResponseFrame delete(String id) {
		ResponseFrame frame = new ResponseFrame();
		bizApplySimpleDao.delete(id);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public ResponseFrame startProcess(BizApplySimple bizApplySimple) {
		LOGGER.info("开始申请...");
		ResponseFrame frame = new ResponseFrame();

		String id = FrameNoUtil.uuid();

		//启动流程
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", bizApplySimple.getCreateUserId());
		/*Long dept = 1l;//userRepo.findDeptById(applySimple.getInsertBy());
		log.info(dept.toString());
		params.put("dept", dept);*/
		params.put("id", id);
		flowProcessService.start(PROCESS_NAME, params);

		//保存业务数据
		bizApplySimple.setId(id);
		bizApplySimple.setCreateTime(FrameTimeUtil.getTime());
		bizApplySimpleDao.save(bizApplySimple);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public ResponseFrame checkProcess(String createUserId, String taskId,
			Integer status) {
		ResponseFrame frame = null;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("status", status);
		//状态[20审核通过、30审核不通过(驳回)]
		if(status.intValue() == 20) {
			frame = flowTaskService.pass(taskId, params);
		} else if(status.intValue() == 30) {
			//审核不通过
			frame = flowTaskService.back(taskId, params);
		}
		return frame;
	}

	@Override
	public ResponseFrame updateStatus(String id, Integer status) {
		ResponseFrame frame = new ResponseFrame();
		bizApplySimpleDao.updateStatus(id, status);
		frame.setSucc();
		return frame;
	}

	@Override
	public ResponseFrame endProcessInstance(String processInstanceId) {
		Map<String, Object> params = new HashMap<String, Object>();
		ResponseFrame frame = flowProcessService.end(processInstanceId, params);
		frame.setSucc();
		return frame;
	}
}