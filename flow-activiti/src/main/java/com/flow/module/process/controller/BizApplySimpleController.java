package com.flow.module.process.controller;

import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flow.module.process.pojo.BizApplySimple;
import com.flow.module.process.service.BizApplySimpleService;
import com.flow.module.sys.service.FlowProcessService;
import com.flow.module.sys.service.FlowUserService;
import com.monitor.api.ApiInfo;
import com.monitor.api.ApiParam;
import com.monitor.api.ApiRes;
import com.system.comm.model.Orderby;
import com.system.comm.utils.FrameJsonUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * biz_apply_simple的Controller
 * @author autoCode
 * @date 2017-12-29 10:14:11
 * @version V1.0.0
 */
@RestController
public class BizApplySimpleController {

	private static final Logger LOGGER = LoggerFactory.getLogger(BizApplySimpleController.class);

	@Autowired
	private BizApplySimpleService bizApplySimpleService;
	@Autowired
	private FlowUserService flowUserService;
	@Autowired
	private FlowProcessService flowProcessService;

	@RequestMapping(name = "简单流程-启动", value = "/bizApplySimple/start")
	@ApiInfo(params = {
			@ApiParam(name="系统编码", code="sysNo", value=""),
			@ApiParam(name="用户编码", code="userId", value=""),
			@ApiParam(name="内容", code="content", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
	})
	public ResponseFrame start(String sysNo, String userId, String content) {
		try {
			String createUserId = flowUserService.getId(sysNo, userId);
			ResponseFrame frame = null;
			//创建请假申请
			BizApplySimple bizApplySimple = new BizApplySimple();
			bizApplySimple.setCreateUserId(createUserId);
			bizApplySimple.setContent(content);
			bizApplySimple.setStatus(10);
			//启动请假流程
			frame = bizApplySimpleService.startProcess(bizApplySimple);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(name = "简单流程-终止流程实例", value = "/bizApplySimple/end")
	@ApiInfo(params = {
			@ApiParam(name="系统编码", code="sysNo", value=""),
			@ApiParam(name="用户编码", code="userId", value=""),
			@ApiParam(name="流程实例ID", code="processInstanceId", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
	})
	public ResponseFrame end(String sysNo, String userId, String processInstanceId) {
		try {
			String createUserId = flowUserService.getId(sysNo, userId);
			ResponseFrame frame = new ResponseFrame();
			if(FrameStringUtil.isEmpty(createUserId)) {
				frame.setCode(-2);
				frame.setMessage("用户不存在");
				return frame;
			}
			//结束流程实例
			frame = bizApplySimpleService.endProcessInstance(processInstanceId);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(name = "简单流程-审批", value = "/bizApplySimple/check")
	@ApiInfo(params = {
			@ApiParam(name="系统编码", code="sysNo", value=""),
			@ApiParam(name="用户编码", code="userId", value=""),
			@ApiParam(name="状态[10待审核、20审核通过、30审核不通过]", code="status", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
	})
	public ResponseFrame check(String sysNo, String userId, String taskId, Integer status) {
		try {
			String createUserId = flowUserService.getId(sysNo, userId);
			ResponseFrame frame = null;
			//审批流程
			frame = bizApplySimpleService.checkProcess(createUserId, taskId, status);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(name = "获取详细信息", value = "/bizApplySimple/get")
	@ApiInfo(params = {
			@ApiParam(name="id", code="id", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
	})
	public ResponseFrame get(String id) {
		try {
			ResponseFrame frame = new ResponseFrame();
			frame.setBody(bizApplySimpleService.get(id));
			frame.setCode(ResponseCode.SUCC.getCode());
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(name = "分页查询信息", value = "/bizApplySimple/pageQuery")
	@ApiInfo(params = {
			@ApiParam(name="页面", code="page", value="1"),
			@ApiParam(name="每页大小", code="size", value="10"),
			@ApiParam(name="排序[{\"property\": \"createTime\", \"type\":\"desc\", \"order\":1}]", code="orderby", value="", required=false),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
			@ApiRes(name="当前页码", code="page", pCode="body", clazz=Integer.class, value="1"),
			@ApiRes(name="每页大小", code="size", pCode="body", clazz=Integer.class, value="10"),
			@ApiRes(name="总页数", code="totalPage", pCode="body", clazz=Integer.class, value="5"),
			@ApiRes(name="总记录数", code="total", pCode="body", clazz=Integer.class, value="36"),
			@ApiRes(name="数据集合", code="rows", pCode="body", clazz=List.class, value=""),
			@ApiRes(name="id", code="id", pCode="rows", value=""),
	})
	public ResponseFrame pageQuery(BizApplySimple bizApplySimple, String orderby) {
		try {
			if(FrameStringUtil.isNotEmpty(orderby)) {
				List<Orderby> orderbys = FrameJsonUtil.toList(orderby, Orderby.class);
				bizApplySimple.setOrderbys(orderbys);
			}
			ResponseFrame frame = bizApplySimpleService.pageQuery(bizApplySimple);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(name = "根据主键删除", value = "/bizApplySimple/delete")
	@ApiInfo(params = {
			@ApiParam(name="id", code="id", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
	})
	public ResponseFrame delete(String id) {
		try {
			ResponseFrame frame = bizApplySimpleService.delete(id);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(name = "根据流程实例ID获取图片", value = "/bizApplySimple/img")
	@ApiInfo(params = {
			@ApiParam(name="系统编码", code="sysNo", value=""),
			@ApiParam(name="用户编码", code="userId", value=""),
			@ApiParam(name="流程实例ID", code="processInstanceId", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
	})
	public void graphImg(HttpServletResponse response, String processInstanceId) throws Exception {
		/*Command<InputStream> cmd = new HistoryProcessInstanceDiagramCmd(
				processInstanceId);*/

		InputStream is = flowProcessService.diagramImg(processInstanceId);
		response.setContentType("image/png");

		int len = 0;
		byte[] b = new byte[1024];

		while ((len = is.read(b, 0, 1024)) != -1) {
			response.getOutputStream().write(b, 0, len);
		}
		is.close();
	}
}