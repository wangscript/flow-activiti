package com.flow.module.process.service;

import org.springframework.stereotype.Component;

import com.flow.module.process.pojo.BizApplySimple;
import com.system.handle.model.ResponseFrame;

/**
 * biz_apply_simple的Service
 * @author autoCode
 * @date 2017-12-29 10:14:11
 * @version V1.0.0
 */
@Component
public interface BizApplySimpleService {
	
	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	public BizApplySimple get(String id);

	/**
	 * 分页获取对象
	 * @param bizApplySimple
	 * @return
	 */
	public ResponseFrame pageQuery(BizApplySimple bizApplySimple);
	
	/**
	 * 根据id删除对象
	 * @param id
	 * @return
	 */
	public ResponseFrame delete(String id);
	/**
	 * 创建和启动流程
	 * @param bizApplySimple
	 * @return
	 */
	public ResponseFrame startProcess(BizApplySimple bizApplySimple);
	/**
	 * 审批流程
	 * @param createUserId
	 * @param taskId
	 * @param status
	 * @return
	 */
	public ResponseFrame checkProcess(String createUserId, String taskId,
			Integer status);
	/**
	 * 修改状态
	 * @param id
	 * @param status
	 * @return
	 */
	public ResponseFrame updateStatus(String id, Integer status);

	/**
	 * 结束流程实例
	 * @param processInstanceId
	 * @return
	 */
	public ResponseFrame endProcessInstance(String processInstanceId);
}