package com.flow.module.process.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * biz_apply_simple实体
 * @author autoCode
 * @date 2017-12-29 10:14:11
 * @version V1.0.0
 */
@Alias("bizApplySimple")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class BizApplySimple extends BaseEntity implements Serializable {
	//编号
	private String id;
	//内容
	private String content;
	//状态[10待审核、20审核通过、30审核不通过]
	private Integer status;
	//创建时间
	private Date createTime;
	//创建人
	private String createUserId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public String getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
}