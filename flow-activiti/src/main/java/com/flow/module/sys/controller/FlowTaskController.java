package com.flow.module.sys.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flow.module.sys.pojo.FlowTask;
import com.flow.module.sys.service.FlowTaskService;
import com.flow.module.sys.service.FlowUserService;
import com.monitor.api.ApiInfo;
import com.monitor.api.ApiParam;
import com.monitor.api.ApiRes;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * Task的Controller
 * @author autoCode
 * @date 2017-12-29 10:14:11
 * @version V1.0.0
 */
@RestController
public class FlowTaskController {

	private static final Logger LOGGER = LoggerFactory.getLogger(FlowTaskController.class);

	@Autowired
	private FlowTaskService flowTaskService;
	@Autowired
	private FlowUserService flowUserService;
	
	@RequestMapping(name = "任务-获取我的审批任务", value = "/flowTask/findTask")
	@ApiInfo(params = {
			@ApiParam(name="系统编码", code="sysNo", value=""),
			@ApiParam(name="用户编码", code="userId", value=""),
			@ApiParam(name="流程的key[不传默认获取所有]", code="processKey", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
	})
	public ResponseFrame findTask(String sysNo, String userId, String processKey) {
		try {
			String candidateUser = flowUserService.getId(sysNo, userId);
			ResponseFrame frame = new ResponseFrame();
			//审批流程
			List<FlowTask> list = flowTaskService.findTask(candidateUser, processKey);
			frame.setBody(list);
			frame.setSucc();
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}
	
}