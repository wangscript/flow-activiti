package com.flow.module.sys.service;

import org.springframework.stereotype.Component;

import com.flow.module.sys.pojo.FlowUser;
import com.system.handle.model.ResponseFrame;

/**
 * flow_user的Service
 * @author autoCode
 * @date 2017-12-29 10:12:19
 * @version V1.0.0
 */
@Component
public interface FlowUserService {
	
	/**
	 * 保存或修改
	 * @param flowUser
	 * @return
	 */
	public ResponseFrame saveOrUpdate(FlowUser flowUser);
	
	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	public FlowUser get(String id);

	/**
	 * 分页获取对象
	 * @param flowUser
	 * @return
	 */
	public ResponseFrame pageQuery(FlowUser flowUser);
	
	/**
	 * 根据id删除对象
	 * @param id
	 * @return
	 */
	public ResponseFrame delete(String id);
	/**
	 * 根据来源系统和用户编号获取系统的用户编码
	 * @param sysNo
	 * @param userId
	 * @return
	 */
	public String getId(String sysNo, String userId);

	public FlowUser getBySysNoUserId(String sysNo, String userId);
}