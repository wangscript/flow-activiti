drop table if exists biz_apply_simple;

drop table if exists flow_user;

/*==============================================================*/
/* Table: biz_apply_simple                                      */
/*==============================================================*/
create table biz_apply_simple
(
   id                   varchar(32) not null comment '编号',
   content              text not null comment '内容',
   status               int not null comment '状态',
   create_time          datetime not null comment '创建时间',
   create_user_id       varchar(32) not null comment '创建人',
   primary key (id)
);

alter table biz_apply_simple comment '简单流程业务数据表';

/*==============================================================*/
/* Table: flow_user                                             */
/*==============================================================*/
create table flow_user
(
   id                   varchar(32) not null comment '编码',
   sys_no               varchar(32) not null comment '来源系统',
   user_id              varchar(32) not null comment '用户编码',
   name                 varchar(80) not null comment '姓名',
   emp_code             varchar(50) comment '员工编码',
   create_time          datetime not null comment '创建时间',
   primary key (id)
);

alter table flow_user comment '流程用户表';
